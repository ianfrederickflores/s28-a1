async function fetchData(){
	let result = await fetch("https://jsonplaceholder.typicode.com/todos/");

	let json = await result.json();

	let map = await json.map(function(json){
		return json.title;
	});

	console.log(map);
};

fetchData();

let object = fetch("https://jsonplaceholder.typicode.com/todos/1").then((response => response.json())).then((json => console.log(json)));

async function fetchData1(){
	let result = await fetch("https://jsonplaceholder.typicode.com/todos/1");

	let json = await result.json();

	console.log("The item " + json.title + " on the list has a status of " + json.completed);
	
};

fetchData1();

fetch("https://jsonplaceholder.typicode.com/todos/",{
	method: "POST",
	headers: {
		"Content-Type": "application/json"
	},
	body:JSON.stringify({
		title: "Created To Do List",
		userId: 1
	})
}).then((response) => response.json()).then((json) => console.log(json));

fetch("https://jsonplaceholder.typicode.com/todos/1",{
	method: "PUT",
	headers: {
		"Content-Type": "application/json"
	},
	body:JSON.stringify({
			id: 1,
			title: "Updated To Do List",
			description: "To update my to do list with a differant data structure",
			status: "Pending",
			dateCompleted: "Pending",
			userId: 1
		})
}).then((response) => response.json()).then((json) => console.log(json));

fetch("https://jsonplaceholder.typicode.com/todos/1",{
	method: "PUT",
	headers: {
		"Content-Type": "application/json"
	},
	body:JSON.stringify({
			status: "Complete",
		})
}).then((response) => response.json()).then((json) => console.log(json));

fetch("https://jsonplaceholder.typicode.com/todos/1",{
	method: "DELETE"
});